from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from app.internal.models.account import Account
from app.internal.models.user import User
from app.internal.models.card import Card
from app.internal.models.payment_operation import PaymentOperation


@admin.register(User)
class CustomUserAdmin(UserAdmin):
    fieldsets = None


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    pass


@admin.register(Card)
class CardAdmin(admin.ModelAdmin):
    pass


@admin.register(PaymentOperation)
class PaymentOperationAdmin(admin.ModelAdmin):
    pass
