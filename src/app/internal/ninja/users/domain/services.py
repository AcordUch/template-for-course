from app.internal.ninja.users.domain.entities import UserOut, UserIn


class IUserRepository:
    def get_user_by_tg_id(self, tg_id: int) -> UserOut:
        pass


class UserService:
    def __init__(self, user_repository: IUserRepository):
        self._user_repository = user_repository

    def get_user_by_tg_id(self, tg_id: int) -> UserOut:
        return self._user_repository.get_user_by_tg_id(tg_id)
