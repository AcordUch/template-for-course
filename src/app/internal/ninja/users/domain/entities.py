from ninja.orm import create_schema

from app.internal.models.user import User

UserSchema = create_schema(
    User,
    exclude=['id', 'last_login',  'is_superuser', 'is_staff', 'is_active', 'groups', 'user_permissions', 'password', 'favorites', 'date_joined']
)


class UserOut(UserSchema):
    pass


class UserIn(UserSchema):
    pass
