from app.internal.ninja.users.domain.services import IUserRepository
from app.internal.ninja.users.domain.entities import UserOut, UserIn
from app.internal.services.DBIO import DBIO
from app.internal.ninja.common import NotFoundException


class UserRepository(IUserRepository):
    def get_user_by_tg_id(self, tg_id: int) -> UserOut:
        user = DBIO.get_user_by_tg_id(tg_id)

        if user is None:
            raise NotFoundException(tg_id)

        return UserOut.from_orm(user)
