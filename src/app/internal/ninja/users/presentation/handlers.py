from django.http import HttpRequest

from app.internal.ninja.users.domain.services import UserService
from app.internal.ninja.users.domain.entities import UserOut


class UserHandler:

    def __init__(self, user_service: UserService):
        self._user_service = user_service

    def get_user_by_tg_id(self, request: HttpRequest, tg_id: int) -> UserOut:
        user = self._user_service.get_user_by_tg_id(tg_id)
        return user
