from ninja import Router

from app.internal.ninja.users.presentation.handlers import UserHandler
from app.internal.ninja.users.domain.entities import UserOut


def decorator(handler: UserHandler):
    def get_user_by_tg_id(request, tg_id: int):
        return handler.get_user_by_tg_id(request, tg_id)

    return get_user_by_tg_id


class UsersRouter:
    @staticmethod
    def get_users_router(handler: UserHandler) -> Router:
        router = Router(tags=["Users"])

        router.add_api_operation(
            '/{int:tg_id}',
            ["GET"],
            decorator(handler),
            response={200: UserOut}
        )

        return router
