from django.urls import path

from app.internal.ninja.app import ninja_api

urlpatterns = [
    path('', ninja_api.urls),
]
