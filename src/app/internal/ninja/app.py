from ninja import NinjaAPI

from app.internal.ninja.users.db.repositories import UserRepository
from app.internal.ninja.cards.db.repositories import CardRepository
from app.internal.ninja.accounts.db.repositories import AccountRepository
from app.internal.ninja.users.domain.services import UserService
from app.internal.ninja.cards.domain.services import CardService
from app.internal.ninja.accounts.domain.services import AccountService
from app.internal.ninja.users.presentation.handlers import UserHandler
from app.internal.ninja.cards.presentation.handlers import CardHandler
from app.internal.ninja.accounts.presentation.handlers import AccountHandler
from app.internal.ninja.users.presentation.routers import UsersRouter
from app.internal.ninja.cards.presentation.routers import CardRouter
from app.internal.ninja.accounts.presentation.routers import AccountsRouter
from app.internal.ninja.common import NotFoundException


class RepositoryProvider:
    def __init__(self):
        self.user_repository = UserRepository()
        self.card_repository = CardRepository()
        self.account_repository = AccountRepository()


class ServiceProvider:
    def __init__(self, repository: RepositoryProvider):
        self.user_service = UserService(repository.user_repository)
        self.card_service = CardService(repository.card_repository)
        self.account_service = AccountService(repository.account_repository)


class HandlerProvider:
    def __init__(self, service_provider: ServiceProvider):
        self.user_handler = UserHandler(service_provider.user_service)
        self.card_handler = CardHandler(service_provider.card_service)
        self.account_handler = AccountHandler(service_provider.account_service)


def _link_router(api: NinjaAPI, handler_provider: HandlerProvider) -> None:
    users_router = UsersRouter.get_users_router(handler_provider.user_handler)
    api.add_router("/users", users_router)

    card_router = CardRouter.get_cards_router(handler_provider.card_handler)
    api.add_router("/cards", card_router)

    account_router = AccountsRouter.get_account_router(handler_provider.account_handler)
    api.add_router("/accounts", account_router)


def get_api():
    api = NinjaAPI(
        title="DT.EDU.BACKEND",
        version="0.1.0"
    )

    repository_provider = RepositoryProvider()
    service_provider = ServiceProvider(repository_provider)
    handler_provider = HandlerProvider(service_provider)

    _link_router(api, handler_provider)

    return api


ninja_api = get_api()


@ninja_api.exception_handler(NotFoundException)
def object_not_found_exception_handler(request, exc):
    return ninja_api.create_response(
        request,
        {"message": f"object {exc.id} not found"},
        status=404,
    )
