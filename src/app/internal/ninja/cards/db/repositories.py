from app.internal.ninja.cards.domain.entities import CardOut, CardIn
from app.internal.services.DBIO import DBIO
from app.internal.ninja.cards.domain.services import ICardRepository
from app.internal.models.card import Card


class CardRepository(ICardRepository):
    def get_cards_by_tg_id(self, tg_id: int) -> list[CardOut]:
        def _map_card(card: Card) -> CardOut:
            return CardOut(
                tg=card.tg.tg_id,
                account=card.account.number,
                card_number=card.card_number,
                expiration_date=card.expiration_date,
                csc=card.csc,
            )

        cards = DBIO.get_all_cards_by_tg_id(tg_id)

        return list(map(_map_card, cards))

    def add_card(self, card_data: CardIn) -> None:
        user = DBIO.get_user_by_tg_id(card_data.tg_id)
        account = DBIO.get_account_by_number(card_data.account_number)

        DBIO.add_card(
            Card(
                tg=user,
                account=account,
                card_number=card_data.card_number,
                expiration_date=card_data.expiration_date,
                csc=card_data.csc
            )
        )

    def update_or_create(self, card_data: CardIn) -> None:
        user = DBIO.get_user_by_tg_id(card_data.tg_id)
        account = DBIO.get_account_by_number(card_data.account_number)
        card = DBIO.get_card_by_card_number(card_data.card_number)

        if card is None:
            DBIO.add_card(
                Card(
                    tg=user,
                    account=account,
                    card_number=card_data.card_number,
                    expiration_date=card_data.expiration_date,
                    csc=card_data.csc
                )
            )
        else:
            DBIO.add_card(
                Card(
                    id=card.id,
                    tg=user,
                    account=account,
                    card_number=card_data.card_number,
                    expiration_date=card_data.expiration_date,
                    csc=card_data.csc
                )
            )

    def delete_card(self, card_number: str) -> None:
        DBIO.delete_card_by_number(card_number)
