from app.internal.ninja.cards.domain.entities import CardOut, CardIn


class ICardRepository:
    def get_cards_by_tg_id(self, tg_id: int) -> list[CardOut]:
        pass

    def add_card(self, card_data: CardIn) -> None:
        pass

    def update_or_create(self, card_data: CardIn) -> None:
        pass

    def delete_card(self, card_number: str) -> None:
        pass


class CardService:
    def __init__(self, card_repository: ICardRepository):
        self._card_repository = card_repository

    def get_card_by_tg_id(self, tg_id: int) -> list[CardOut]:
        return self._card_repository.get_cards_by_tg_id(tg_id)

    def add_card(self, card_data: CardIn) -> None:
        self._card_repository.add_card(card_data)

    def update_or_create(self, card_data: CardIn) -> None:
        self._card_repository.update_or_create(card_data)

    def delete_card(self, card_number: str) -> None:
        self._card_repository.delete_card(card_number)
