from ninja import Schema
from pydantic import Field


class CardSchema(Schema):
    tg_id: int = Field()
    account_number: str = Field()
    card_number: str = Field()
    expiration_date: str = Field()
    csc: str = Field()


class CardOut(CardSchema):
    pass


class CardIn(CardSchema):
    pass
