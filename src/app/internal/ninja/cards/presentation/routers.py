from ninja import Router

from app.internal.ninja.cards.presentation.handlers import CardHandler
from app.internal.ninja.cards.domain.entities import CardOut, CardIn


def decorator_get_card_by_tg_id(handler: CardHandler):
    def get_card_by_tg_id(request, tg_id: int):
        return handler.get_card_by_tg_id(request, tg_id)

    return get_card_by_tg_id


def decorator_add_card(handler: CardHandler):
    def add_card(request, card: CardIn):
        return handler.add_card(request, card)

    return add_card


def decorator_update_or_create(handler: CardHandler):
    def update_or_create(request, card: CardIn):
        return handler.update_or_create(request, card)

    return update_or_create


def decorator_delete_card(handler: CardHandler):
    def delete_card(request, card_number: str):
        return handler.delete_card(request, card_number)

    return delete_card


class CardRouter:
    @staticmethod
    def get_cards_router(handler: CardHandler) -> Router:
        router = Router(tags=["Cards"])

        router.add_api_operation(
            '/{int:tg_id}',
            ["GET"],
            decorator_get_card_by_tg_id(handler),
            response={200: list[CardOut]}
        )

        router.add_api_operation(
            '/put',
            ["PUT"],
            decorator_update_or_create(handler),
            response={200: None}
        )

        router.add_api_operation(
            '/',
            ["POST"],
            decorator_add_card(handler),
            response={200: None}
        )

        router.add_api_operation(
            '/rm/{str:card_number}',
            ["DELETE"],
            decorator_delete_card(handler),
            response={200: None}
        )

        return router
