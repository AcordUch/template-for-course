from django.http import HttpRequest

from app.internal.ninja.cards.domain.services import CardService
from app.internal.ninja.cards.domain.entities import CardIn, CardOut


class CardHandler:

    def __init__(self, card_service: CardService):
        self._card_service = card_service

    def get_card_by_tg_id(self, request: HttpRequest, tg_id: int) -> list[CardOut]:
        cards = self._card_service.get_card_by_tg_id(tg_id)
        return cards

    def add_card(self, request: HttpRequest, card: CardIn):
        self._card_service.add_card(card)

    def update_or_create(self, request: HttpRequest, card: CardIn):
        self._card_service.update_or_create(card)

    def delete_card(self, request: HttpRequest, card_number: str):
        self._card_service.delete_card(card_number)
