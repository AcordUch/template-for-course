from app.internal.ninja.accounts.domain.entities import AccountIn, AccountOut


class IAccountRepository:
    def get_accounts_by_tg_id(self, tg_id: int) -> list[AccountOut]:
        pass

    def add_account(self, account_data: AccountIn) -> None:
        pass


class AccountService:
    def __init__(self, account_repository: IAccountRepository):
        self._account_repository = account_repository

    def get_accounts_by_tg_id(self, tg_id: int) -> list[AccountOut]:
        return self._account_repository.get_accounts_by_tg_id(tg_id)

    def add_account(self, card_data: AccountIn) -> None:
        self._account_repository.add_account(card_data)
