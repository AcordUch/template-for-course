from decimal import Decimal

from ninja import Schema
from pydantic import Field


class AccountSchema(Schema):
    tg_id: int = Field()
    number: str = Field()
    name: str = Field()
    balance: Decimal = Field()


class AccountOut(AccountSchema):
    pass


class AccountIn(AccountSchema):
    pass
