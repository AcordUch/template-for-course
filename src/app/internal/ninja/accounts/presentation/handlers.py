from django.http import HttpRequest

from app.internal.ninja.accounts.domain.services import AccountService
from app.internal.ninja.accounts.domain.entities import AccountIn, AccountOut


class AccountHandler:

    def __init__(self, account_service: AccountService):
        self._account_service = account_service

    def get_accounts_by_tg_id(self, request: HttpRequest, tg_id: int) -> list[AccountOut]:
        cards = self._account_service.get_accounts_by_tg_id(tg_id)
        return cards

    def add_account(self, request: HttpRequest, card: AccountIn):
        self._account_service.add_account(card)
