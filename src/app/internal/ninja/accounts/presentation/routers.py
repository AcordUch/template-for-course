from ninja import Router

from app.internal.ninja.accounts.presentation.handlers import AccountHandler
from app.internal.ninja.accounts.domain.entities import AccountIn, AccountOut


def decorator_get_account_by_tg_id(handler: AccountHandler):
    def get_account_by_tg_id(request, tg_id: int):
        return handler.get_accounts_by_tg_id(request, tg_id)

    return get_account_by_tg_id


def decorator_add_account(handler: AccountHandler):
    def add_card(request, card: AccountIn):
        return handler.add_account(request, card)

    return add_card


class AccountsRouter:
    @staticmethod
    def get_account_router(handler: AccountHandler) -> Router:
        router = Router(tags=["Accounts"])

        router.add_api_operation(
            '/{int:tg_id}',
            ["GET"],
            decorator_get_account_by_tg_id(handler),
            response={200: list[AccountOut]}
        )

        router.add_api_operation(
            '/',
            ["POST"],
            decorator_add_account(handler),
            response={200: None}
        )

        return router
