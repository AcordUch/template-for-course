from app.internal.ninja.accounts.domain.entities import AccountOut, AccountIn
from app.internal.services.DBIO import DBIO
from app.internal.ninja.accounts.domain.services import IAccountRepository
from app.internal.models.account import Account


class AccountRepository(IAccountRepository):
    def get_accounts_by_tg_id(self, tg_id: int) -> list[AccountOut]:
        def _map_account(account: Account) -> AccountOut:
            return AccountOut(
                tg_id=account.tg.tg_id,
                number=account.number,
                name=account.name,
                balance=account.balance,
            )

        cards = DBIO.get_all_accounts_by_tg_id(tg_id)

        return list(map(_map_account, cards))

    def add_account(self, account_data: AccountIn) -> None:
        user = DBIO.get_user_by_tg_id(account_data.tg_id)

        DBIO.add_account(
            Account(
                tg=user,
                number=account_data.number,
                name=account_data.name,
                balance=account_data.balance
            )
        )

