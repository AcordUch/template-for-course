from http import HTTPStatus
from json import loads as json_loads

from django.utils.decorators import method_decorator
from django.http import HttpResponse, HttpRequest
from django.views.decorators.csrf import csrf_exempt
from django.views import View
from django.conf import settings

import app.internal.services.formattor as formattor
import app.resources.rest as rest_texts
from app.internal.services.password_service import PasswordService
from app.internal.services.DBIO import DBIO


@method_decorator(csrf_exempt, name='dispatch')
class UserDetailView(View):
    _password_service = PasswordService(settings.PASSWORD_SALT)

    def get(self, request: HttpRequest, tg_id: int) -> HttpResponse:
        if not request.is_authenticated:
            return HttpResponse("Вы не авторизованы. Войдите в систему", status=HTTPStatus.UNAUTHORIZED)

        if tg_id is None:
            return HttpResponse(rest_texts.USER_INFO__MISSING_ID, status=HTTPStatus.BAD_REQUEST)

        user = DBIO.get_user_by_tg_id(tg_id)
        if user is None:
            return HttpResponse(rest_texts.USER_INFO__MISSING_USER, status=HTTPStatus.NOT_FOUND)

        return HttpResponse(formattor.info_about_user(user))

    def patch(self, request: HttpRequest, tg_id: int) -> HttpResponse:
        if not request.is_authenticated:
            return HttpResponse("Вы не авторизованы. Войдите в систему", status=HTTPStatus.UNAUTHORIZED)

        try:
            body = json_loads(request.body.decode())

            username: str | None = self._get_json_field(body, "username")
            password: str | None = self._get_json_field(body, "password")
            first_name: str | None = self._get_json_field(body, "first_name")
            last_name: str | None = self._get_json_field(body, "last_name")
            full_name: str | None = self._get_json_field(body, "full_name")
            link: str | None = self._get_json_field(body, "link")
            phone: str | None = self._get_json_field(body, "phone")
            email: str | None = self._get_json_field(body, "email")
        except ValueError:
            return HttpResponse("Can't parse body", status=HTTPStatus.BAD_REQUEST)

        if password is not None:
            password = self._password_service.hashing(password)

        DBIO.patch_user(
            tg_id, username, password,
            first_name, last_name, full_name,
            link, phone, email
        )

        return HttpResponse("Update - ok", status=HTTPStatus.OK)

    @staticmethod
    def _get_json_field(body: dict[str, any], key: str) -> str | None:
        try:
            return str(body[key])
        except KeyError as error:
            return None
