import json
from http import HTTPStatus

from django.utils.decorators import method_decorator
from django.http import HttpResponse, JsonResponse, HttpRequest
from django.views import View
from django.views.decorators.csrf import csrf_exempt

from app.internal.services.DBIO import DBIO
from app.internal.models.jwt_tokens import AccessToken, RefreshToken


@method_decorator(csrf_exempt, name='dispatch')
class UpdateTokensView(View):
    def post(self, request: HttpRequest) -> HttpResponse:
        try:
            body = json.loads(request.body.decode())

            raw_token: str = str(body['refresh_token'])
        except ValueError:
            return HttpResponse("Can't parse body", status=HTTPStatus.BAD_REQUEST)
        except KeyError as e:
            return HttpResponse(f"Can't get property: {str(e)}", status=HTTPStatus.BAD_REQUEST)

        refresh_token, ok = RefreshToken.parse(raw_token)

        if not ok:
            return HttpResponse("Can't parse refresh token", status=HTTPStatus.UNAUTHORIZED)

        if refresh_token.is_expired():
            DBIO.revoked_jwt_token(refresh_token.access_token_jti)
            return HttpResponse("Refresh token expired. Login again.", status=HTTPStatus.UNAUTHORIZED)

        if not DBIO.check_jwt_token_valid(refresh_token.jti):
            return HttpResponse("Refresh token revoked. Login again.", status=HTTPStatus.UNAUTHORIZED)

        DBIO.revoked_jwt_token(refresh_token.access_token_jti)
        DBIO.revoked_jwt_token(refresh_token.jti)

        access_jwt = AccessToken(refresh_token.id)
        refresh_jwt = RefreshToken(refresh_token.id, access_jwt.jti)

        DBIO.add_jwt_token(access_jwt)
        DBIO.add_jwt_token(refresh_jwt)

        encoded_access_jwt = access_jwt.encode()
        encoded_refresh_jwt = refresh_jwt.encode()

        return JsonResponse(
            {
                "access_token": encoded_access_jwt,
                "refresh_token": encoded_refresh_jwt
            }
        )
