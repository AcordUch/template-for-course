import json
from http import HTTPStatus

from django.utils.decorators import method_decorator
from django.http import HttpResponse, JsonResponse, HttpRequest
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from app.internal.services.DBIO import DBIO
from app.internal.services.password_service import PasswordService
from app.internal.models.jwt_tokens import AccessToken, RefreshToken


@method_decorator(csrf_exempt, name='dispatch')
class LoginView(View):
    _password_service = PasswordService(settings.PASSWORD_SALT)

    def post(self, request: HttpRequest) -> HttpResponse:
        try:
            body = json.loads(request.body.decode())

            user_id: str = str(body['id'])
            password: str = str(body['password'])
        except ValueError:
            return HttpResponse("Can't parse body", status=HTTPStatus.BAD_REQUEST)
        except KeyError as e:
            return HttpResponse(f"Can't get property: {str(e)}", status=HTTPStatus.BAD_REQUEST)

        hashed = self._password_service.hashing(password)
        user = DBIO.get_user_by_tg_id(int(user_id))

        if not user:
            return HttpResponse("Данный пользователь не найден", status=HTTPStatus.NOT_FOUND)

        if user.password != hashed:
            return HttpResponse("Введён не правильный пароль", status=HTTPStatus.BAD_REQUEST)

        DBIO.revoked_jwt_token_by_tg_id(user.tg_id)

        access_jwt = AccessToken(user_id)
        refresh_jwt = RefreshToken(user_id, access_jwt.jti)

        DBIO.add_jwt_token(access_jwt)
        DBIO.add_jwt_token(refresh_jwt)

        encoded_access_jwt = access_jwt.encode()
        encoded_refresh_jwt = refresh_jwt.encode()

        return JsonResponse(
            {
                "access_token": encoded_access_jwt,
                "refresh_token": encoded_refresh_jwt
            }
        )
