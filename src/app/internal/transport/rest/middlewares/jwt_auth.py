from http import HTTPStatus

from django.contrib.auth import get_user_model
from django.http import HttpRequest, HttpResponse

from app.internal.models.jwt_tokens import AccessToken
from app.internal.services.DBIO import DBIO


class JWTAuthenticationMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request: HttpRequest):
        auth_token = request.META.get("HTTP_AUTHORIZATION")

        if auth_token is None:
            print(f"Authorization header missing")

            request.jwt_user = None
            request.is_authenticated = False

            return self.get_response(request)

        token, ok = AccessToken.parse(auth_token)
        if not ok:
            request.jwt_user = None
            request.is_authenticated = False

            return self.get_response(request)

        if token.is_expired():
            return HttpResponse("Access token expired", status=HTTPStatus.UNAUTHORIZED)

        if not DBIO.check_jwt_token_valid(token.jti):
            return HttpResponse("Access token revoked", status=HTTPStatus.UNAUTHORIZED)

        if not DBIO.check_jwt_is_access_token(token.jti):
            request.jwt_user = None
            request.is_authenticated = False

            return self.get_response(request)

        user = get_user_model().objects.get(tg_id=token.id)

        request.jwt_user = user
        request.is_authenticated = True

        return self.get_response(request)
