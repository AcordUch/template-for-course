from functools import wraps

from asgiref.sync import sync_to_async
from telegram import Update
from telegram.ext import CallbackContext

import app.resources.bot as bot_texts
from app.internal.services.DBIO import DBIO


COMMANDS_LIST: list[str] = []


def is_username(username: str) -> bool:
    return username[0].isalpha() and len(username) >= 5


def is_tg_id(tg_id: str) -> bool:
    return tg_id.isdigit()


async def check_on_registered(tg_id: int) -> bool:
    user = await sync_to_async(DBIO.get_user_by_tg_id, thread_sensitive=True)(tg_id)
    return user is not None and user.phone != "None"


def need_log_in(func):
    @wraps(func)
    async def inner(update: Update, context: CallbackContext):
        is_registered = await check_on_registered(update.effective_user.id)
        if is_registered:
            return await func(update, context)
        else:
            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text=bot_texts.REGISTRATION__PHONE_NEED_REQUEST)
            return

    return inner


def check_on_user_exist(func):
    async def check_on_exist(tg_id) -> bool:
        user = await sync_to_async(DBIO.get_user_by_tg_id, thread_sensitive=True)(tg_id)
        return user is not None

    @wraps(func)
    async def inner(update: Update, context: CallbackContext):
        is_exist = await check_on_exist(update.effective_user.id)
        if is_exist:
            await func(update, context)
        else:
            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text=bot_texts.REGISTRATION__UNREGISTERED)
            return

    return inner


def register(command: str):
    COMMANDS_LIST.append(f"\t/{command}")

    def decorator(func):
        @wraps(func)
        async def inner(update: Update, context: CallbackContext):
            print(f"Вызов команды: {command}, Пользователь: {update.effective_user.id}, Имя: {update.effective_user.name}, Аргументы: {' '.join(context.args)}")

            await func(update, context)

        return inner

    return decorator
