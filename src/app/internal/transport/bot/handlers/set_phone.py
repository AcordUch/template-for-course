from re import match

from asgiref.sync import sync_to_async
from telegram import Update
from telegram.ext import CallbackContext

import app.resources.bot as bot_texts
from app.internal.services.DBIO import DBIO
from app.internal.transport.bot.domain import check_on_user_exist, register, check_on_registered
from app.internal.services.password_service import PasswordService


def set_phone(password_service: PasswordService):
    @register("set_phone")
    @check_on_user_exist
    async def do(update: Update, context: CallbackContext):
        try:
            phone = update.message.text.split(' ', 1)[1]
        except IndexError:
            await context.bot.send_message(
                chat_id=update.effective_chat.id,
                text=bot_texts.REGISTRATION__PHONE_INPUT_FORMATE
            )
            return

        if await check_on_registered(update.effective_user.id):
            await context.bot.send_message(
                chat_id=update.effective_chat.id,
                text=bot_texts.REGISTRATION__PHONE_ALREADY_SET
            )
            return

        if match(r'^\+7\d{10}$', phone):
            password = password_service.generate_password()

            await sync_to_async(DBIO.set_phone_to_user, thread_sensitive=True)(update.effective_user.id, phone)
            await sync_to_async(DBIO.set_password_to_user, thread_sensitive=True)(update.effective_user.id, password.hash)

            await context.bot.send_message(chat_id=update.effective_chat.id, text=bot_texts.REGISTRATION__PHONE_SAVE)
            await context.bot.send_message(
                chat_id=update.effective_chat.id,
                text=bot_texts.REGISTRATION__PASSWORD.format(password.password)
            )
        else:
            await context.bot.send_message(
                chat_id=update.effective_chat.id,
                text=bot_texts.REGISTRATION__PHONE_INCORRECT
            )

    return do
