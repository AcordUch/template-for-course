from asgiref.sync import sync_to_async
from telegram import Update
from telegram.ext import CallbackContext

import app.resources.bot as bot_texts
from app.internal.services.DBIO import DBIO
from app.internal.transport.bot.domain import check_on_user_exist, register, need_log_in


@register("balance_account")
@check_on_user_exist
@need_log_in
async def balance_account(update: Update, context: CallbackContext):
    if len(context.args) == 0:
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=bot_texts.ACCOUNT__BALANCE__NEED_ID
        )
        return

    account_number = context.args[0]
    account = await sync_to_async(DBIO.get_account_by_number, thread_sensitive=True)(account_number)
    if account:
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=bot_texts.ACCOUNT__BALANCE.format(account.balance)
        )
    else:
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=bot_texts.ACCOUNT__BALANCE__NOT_EXIST
        )
