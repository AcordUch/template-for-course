from enum import Enum

from asgiref.sync import sync_to_async
from telegram import Update
from telegram.ext import CallbackContext

import app.resources.bot as bot_texts
import app.internal.services.formattor as formattor
from app.internal.services.DBIO import DBIO
from app.internal.transport.bot.domain import check_on_user_exist, register, need_log_in
from app.internal.models.account import Account
from app.internal.models.card import Card
from app.internal.models.payment_operation import PaymentOperation, PaymentStats


class StatementBy(Enum):
    UNSPECIFIED = 0
    ACCOUNT = 1
    CARD = 2

    @staticmethod
    def select_statement_by(by: str):
        if by.isdigit() and len(by) == Card.NUMBER_LENGTH:
            return StatementBy.CARD

        if by.isdigit() and len(by) == Account.NUMBER_LENGTH:
            return StatementBy.ACCOUNT

        return StatementBy.UNSPECIFIED


_target2normal_name = {
        StatementBy.UNSPECIFIED: "Unspecified",
        StatementBy.ACCOUNT: "Счёт",
        StatementBy.CARD: "Карта",
    }

_target2name = {
    StatementBy.UNSPECIFIED: "Unspecified",
    StatementBy.ACCOUNT: "счёту",
    StatementBy.CARD: "карте",
}


@register("statement")
@check_on_user_exist
@need_log_in
async def statement(update: Update, context: CallbackContext):
    if len(context.args) == 0:
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=bot_texts.STATEMENT__ADD__NOT_ENOUGH_ARGS
        )
        return

    number = context.args[0]
    statement_by = StatementBy.select_statement_by(number)
    if statement_by == StatementBy.UNSPECIFIED:
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=bot_texts.STATEMENT__UNSPECIFIED_TARGET
        )
        return

    history = await _get_history(update.effective_user.id, number, statement_by)
    account = await _get_account(number, statement_by)

    if not account:
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=bot_texts.STATEMENT__TARGET_NOT_FOUND.format(_target2normal_name[statement_by])
        )
        return

    payment_stats = PaymentStats().calculate_income_and_expenses(update.effective_user.id, history)

    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=formattor.info_about_statement(account.balance, payment_stats, _target2name[statement_by])
    )


async def _get_history(tg_id: int, number: str, statement_by: StatementBy) -> list[PaymentOperation]:
    if statement_by == StatementBy.CARD:
        return await sync_to_async(
            DBIO.get_records_from_payment_history_by_card_number, thread_sensitive=True
        )(tg_id, number)

    return await sync_to_async(
        DBIO.get_records_from_payment_history_by_account, thread_sensitive=True
    )(tg_id, number)


async def _get_account(number: str, statement_by: StatementBy) -> Account | None:
    if statement_by == StatementBy.CARD:
        card = await sync_to_async(DBIO.get_card_by_card_number, thread_sensitive=True)(number)
        return card.account

    return await sync_to_async(DBIO.get_account_by_number, thread_sensitive=True)(number)