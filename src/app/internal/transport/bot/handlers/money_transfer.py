from decimal import Decimal, InvalidOperation
from enum import Enum
from datetime import datetime

from asgiref.sync import sync_to_async
from telegram import Update
from telegram.ext import CallbackContext

import app.resources.bot as bot_texts
from app.internal.services.DBIO import DBIO
from app.internal.transport.bot.domain import check_on_user_exist, register, need_log_in
from app.internal.models.user import User
from app.internal.models.account import Account
from app.internal.models.card import Card
from app.internal.transport.bot import domain


class TransferType(Enum):
    UNSPECIFIED = 0
    ACCOUNT = 1
    CARD = 2
    USER_NAME = 3

    @staticmethod
    def select_transfer_type(to: str):
        if domain.is_username(to):
            return TransferType.USER_NAME

        if to.isdigit() and len(to) == Card.NUMBER_LENGTH:
            return TransferType.CARD

        if to.isdigit() and len(to) == Account.NUMBER_LENGTH:
            return TransferType.ACCOUNT

        return TransferType.UNSPECIFIED


class AccountCardPair:
    def __init__(self, account: Account | None, card: Card | None = None):
        self.account = account
        self.card = card


@register("money_transfer")
@check_on_user_exist
@need_log_in
async def money_transfer(update: Update, context: CallbackContext):
    if len(context.args) < 2:
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=bot_texts.MONEY_TRANSFER__NOT_ENOUGH_ARGS
        )
        return

    to = context.args[0]
    transfer_type = TransferType.select_transfer_type(to)
    if transfer_type == TransferType.UNSPECIFIED:
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=bot_texts.MONEY_TRANSFER__UNSPECIFIED_TARGET
        )
        return

    try:
        amount = Decimal(context.args[1])
    except InvalidOperation:
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=bot_texts.MONEY_TRANSFER__VALUE_ERROR
        )
        return

    cards = await sync_to_async(DBIO.get_all_cards_by_tg_id, thread_sensitive=True)(update.effective_user.id)
    for card in cards:
        if card.account.balance >= amount:
            account_card_pair = await _get_account_of_target(transfer_type, to)
            target = account_card_pair.account
            if target is None:
                await context.bot.send_message(
                    chat_id=update.effective_chat.id,
                    text=bot_texts.MONEY_TRANSFER__TARGET_NOT_EXIST
                )
                return

            await sync_to_async(DBIO.transfer_money, thread_sensitive=True)(card.account.id, target.id, amount)

            await _add_record_to_payment_history(
                amount,
                card.tg,
                account_card_pair.account.tg,
                card.account,
                card,
                account_card_pair.account,
                account_card_pair.card
            )

            await context.bot.send_message(
                chat_id=update.effective_chat.id,
                text=bot_texts.MONEY_TRANSFER__SUCCESSFUL
            )
            return

    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=bot_texts.MONEY_TRANSFER__NOT_ENOUGH_MONEY
    )


async def _get_account_of_target(transfer_type: TransferType, id: str) -> AccountCardPair | None:
    if transfer_type == TransferType.USER_NAME:
        user = await sync_to_async(DBIO.get_user_by_username, thread_sensitive=True)(id)
        if user:
            account = await sync_to_async(DBIO.get_first_account_by_tg_id, thread_sensitive=True)(user.tg_id)
            return AccountCardPair(account)
        else:
            return None

    if transfer_type == TransferType.CARD:
        card = await sync_to_async(DBIO.get_card_by_card_number, thread_sensitive=True)(id)
        return AccountCardPair(card.account, card)

    account = await sync_to_async(DBIO.get_account_by_number, thread_sensitive=True)(id)
    return AccountCardPair(account)


async def _add_record_to_payment_history(
        amount: Decimal,
        from_user: User,
        to_user: User,
        account_from: Account,
        card_from: Card,
        account_to: Account,
        card_to: Card | None
):
    await sync_to_async(DBIO.add_record_to_payment_history)(
        amount,
        datetime.now(),
        from_user=from_user,
        from_account=account_from,
        from_card=card_from,
        to_user=to_user,
        to_account=account_to,
        to_card=card_to
    )
