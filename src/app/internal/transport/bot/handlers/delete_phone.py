from asgiref.sync import sync_to_async
from telegram import Update
from telegram.ext import CallbackContext

import app.resources.bot as bot_texts
from app.internal.services.DBIO import DBIO
from app.internal.transport.bot.domain import check_on_user_exist, register, need_log_in


@register("delete_phone")
@check_on_user_exist
@need_log_in
async def delete_phone(update: Update, context: CallbackContext):
    await sync_to_async(DBIO.set_phone_to_user, thread_sensitive=True)(update.effective_user.id, "None")
    await sync_to_async(DBIO.set_password_to_user, thread_sensitive=True)(update.effective_user.id, None)
    await context.bot.send_message(chat_id=update.effective_chat.id, text=bot_texts.MAIN_MENU__PHONE_DELETED)
