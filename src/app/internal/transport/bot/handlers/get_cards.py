from asgiref.sync import sync_to_async
from telegram import Update
from telegram.ext import CallbackContext

import app.internal.services.formattor as formattor
from app.internal.services.DBIO import DBIO
from app.internal.transport.bot.domain import check_on_user_exist, register, need_log_in


@register("get_cards")
@check_on_user_exist
@need_log_in
async def get_cards(update: Update, context: CallbackContext):
    cards = await sync_to_async(DBIO.get_all_cards_by_tg_id, thread_sensitive=True)(update.effective_user.id)
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=formattor.info_about_cards(cards)
    )
