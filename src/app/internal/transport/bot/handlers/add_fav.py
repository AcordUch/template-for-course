from asgiref.sync import sync_to_async
from telegram import Update
from telegram.ext import CallbackContext

import app.resources.bot as bot_texts
from app.internal.services.DBIO import DBIO
from app.internal.transport.bot.domain import check_on_user_exist, register, need_log_in
from app.internal.models.user import User
from app.internal.transport.bot import domain


@register("add_fav")
@check_on_user_exist
@need_log_in
async def add_fav(update: Update, context: CallbackContext):
    if len(context.args) == 0:
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=bot_texts.FAVORITE__ADD__NOT_ENOUGH_ARGS
        )
        return

    favorite = await _get_favorite(context.args[0])
    if not favorite:
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=bot_texts.FAVORITE__ADD__NOT_EXIST
        )
        return

    await sync_to_async(DBIO.add_fav, thread_sensitive=True)(update.effective_user.id, favorite)
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=bot_texts.FAVORITE__ADD__SUCCESSFUL
    )


async def _get_favorite(id: str) -> User | None:
    if domain.is_username(id):
        return await sync_to_async(DBIO.get_user_by_username, thread_sensitive=True)(id)

    if domain.is_tg_id(id):
        return await sync_to_async(DBIO.get_user_by_tg_id, thread_sensitive=True)(id)

    return None
