from typing import Callable

from telegram import Update
from telegram.ext import CallbackContext

import app.resources.bot as bot_texts
from app.internal.transport.bot.domain import register


def help(available_commands: Callable[[], str]):
    @register("help")
    async def do(update: Update, context: CallbackContext):
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=bot_texts.HELP_MESSAGE.format(available_commands())
        )

    return do
