from asgiref.sync import sync_to_async
from telegram import Update
from telegram.ext import ContextTypes

import app.resources.bot as bot_texts
from app.internal.services.DBIO import DBIO
from app.internal.transport.bot.domain import register


@register("start")
async def start(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=bot_texts.REGISTRATION__ONBOARDING
    )
    await sync_to_async(DBIO.create_or_update_user, thread_sensitive=True)(
        username=update.effective_user.username,
        tg_id=update.effective_user.id,
        first_name=update.effective_user.first_name,
        last_name=update.effective_user.last_name,
        full_name=update.effective_user.full_name,
        link=update.effective_user.link,
        phone="None"
    )
