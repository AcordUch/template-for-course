from asgiref.sync import sync_to_async
from telegram import Update
from telegram.ext import CallbackContext

import app.resources.bot as bot_texts
import app.internal.services.formattor as formattor
from app.internal.services.DBIO import DBIO
from app.internal.transport.bot.domain import check_on_user_exist, register, need_log_in
from app.internal.models.payment_operation import PaymentStats


@register("balance_history")
@check_on_user_exist
@need_log_in
async def balance_history(update: Update, context: CallbackContext):
    history = await sync_to_async(DBIO.get_records_from_payment_history, thread_sensitive=True)(
        update.effective_user.id
    )
    payment_stats = PaymentStats().calculate_income_and_expenses(update.effective_user.id, history)

    if len(history) == 0:
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=bot_texts.BALANCE_HISTORY__LIST_EMPTY
        )
        return

    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=formattor.info_about_payment_history(payment_stats)
    )
