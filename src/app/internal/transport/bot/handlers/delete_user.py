from asgiref.sync import sync_to_async
from telegram import Update
from telegram.ext import CallbackContext

import app.resources.bot as bot_texts
from app.internal.services.DBIO import DBIO
from app.internal.transport.bot.domain import check_on_user_exist, register


@register("delete_user")
@check_on_user_exist
async def delete_user(update: Update, context: CallbackContext):
    await sync_to_async(DBIO.delete_user_by_id, thread_sensitive=True)(update.effective_user.id)
    await context.bot.send_message(chat_id=update.effective_chat.id, text=bot_texts.MAIN_MENU__USER_DELETED)
