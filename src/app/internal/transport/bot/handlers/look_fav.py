from asgiref.sync import sync_to_async
from telegram import Update
from telegram.ext import CallbackContext

import app.internal.services.formattor as formattor
import app.resources.bot as bot_texts
from app.internal.services.DBIO import DBIO
from app.internal.transport.bot.domain import check_on_user_exist, register, need_log_in


@register("look_fav")
@check_on_user_exist
@need_log_in
async def look_fav(update: Update, context: CallbackContext):
    favorites = await sync_to_async(DBIO.get_favorites, thread_sensitive=True)(update.effective_user.id)
    if len(favorites) == 0:
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=bot_texts.FAVORITE__EMPTY
        )
    else:
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=formattor.info_about_favorites(favorites)
        )
