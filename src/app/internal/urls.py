from django.urls import path

from app.internal.transport.rest.handlers.user_detail import UserDetailView
from app.internal.transport.rest.handlers.login import LoginView
from app.internal.transport.rest.handlers.update_tokens import UpdateTokensView

urlpatterns = [
    path('me/<int:tg_id>', UserDetailView.as_view()),
    path('login/', LoginView.as_view()),
    path('update_tokens/', UpdateTokensView.as_view()),
]
