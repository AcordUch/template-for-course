import uuid
from datetime import datetime, timedelta
from jwt import encode, decode, DecodeError
from typing import TypeVar, Tuple

from app.internal.models.user import User

from django.db import models
from django.conf import settings


TAccessToken = TypeVar("TAccessToken", bound="AccessToken")
TRefreshToken = TypeVar("TRefreshToken", bound="RefreshToken")


class IssuedToken(models.Model):
    class Meta:
        verbose_name = "issued_token"
        verbose_name_plural = "issued_tokens"

    jti = models.CharField(max_length=255, primary_key=True)
    revoked = models.BooleanField(default=False)
    is_access_token = models.BooleanField(default=True)
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        to_field="tg_id"
    )
    expired = models.DateTimeField()

    def __str__(self):
        return f"jti: {self.jti} revoked: {self.revoked}"


class AccessToken:
    TTL = 5 * 60  # 5 minutes

    def __init__(self, id: str, jti: str | None = None, expired: datetime | None = None):
        self.id = id
        self.jti = jti if jti is not None else str(uuid.uuid4())
        self.expired = expired if expired is not None else datetime.now() + timedelta(seconds=self.TTL)
        self.is_access_token = True

    def is_expired(self) -> bool:
        return self.expired < datetime.now()

    def encode(self) -> str:
        return encode(
            {
                "jti": str(self.jti),
                "id": self.id,
                "timestamp": str(datetime.now() + timedelta(seconds=self.TTL))
            },
            key=settings.JWT_TOKEN_SECRET_KEY,
            algorithm="HS256"
        )

    @staticmethod
    def parse(raw_token: str) -> Tuple[TAccessToken, bool]:
        try:
            payload = decode(raw_token, settings.JWT_TOKEN_SECRET_KEY, algorithms=['HS256'])
            timestamp = datetime.fromisoformat(payload['timestamp'])
            token = AccessToken(payload['id'], payload['jti'], timestamp)
            return token, True
        except DecodeError as error:
            print(f"Error while decode jwt token: {error}")
            return None, False
        except KeyError as error:
            print(f"Can't get field with decode jwt token: {error}")
            return None, False


class RefreshToken:
    TTL = 15 * 60  # 5 minutes

    def __init__(self, id: str, access_token_jti: str, jti: str | None = None, expired: datetime | None = None):
        self.id = id
        self.access_token_jti = access_token_jti
        self.jti = jti if jti is not None else str(uuid.uuid4())
        self.expired = expired if expired is not None else datetime.now() + timedelta(seconds=self.TTL)
        self.is_access_token = False

    def is_expired(self) -> bool:
        return self.expired < datetime.now()

    def encode(self) -> str:
        return encode(
            {
                "jti": str(self.jti),
                "access_token_jti": self.access_token_jti,
                "id": self.id,
                "timestamp": str(datetime.now() + timedelta(seconds=self.TTL))
            },
            key=settings.JWT_TOKEN_SECRET_KEY,
            algorithm="HS256"
        )

    @staticmethod
    def parse(raw_token: str) -> Tuple[TRefreshToken, bool]:
        try:
            payload = decode(raw_token, settings.JWT_TOKEN_SECRET_KEY, algorithms=['HS256'])
            timestamp = datetime.fromisoformat(payload['timestamp'])
            token = RefreshToken(payload['id'], payload['access_token_jti'], payload['jti'], timestamp)
            return token, True
        except DecodeError as error:
            print(f"Error while decode jwt token: {error}")
            return None, False
        except KeyError as error:
            print(f"Can't get field with decode jwt token: {error}")
            return None, False