from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    class Meta:
        verbose_name = "tg_User"
        verbose_name_plural = "tg_Users"

    tg_id = models.BigIntegerField(unique=True)
    username = models.CharField(max_length=255, null=True, unique=True)
    password = models.CharField(max_length=255, null=True)
    last_name = models.TextField(blank=True, null=True)
    full_name = models.TextField(blank=True, null=True)
    link = models.TextField(blank=True, null=True)
    phone = models.CharField(max_length=255, blank=True)  # blank что бы можно было удалить номер
    favorites = models.ManyToManyField("self", symmetrical=False, blank=True)

    def __str__(self):
        return f"username: {self.username} name: {self.first_name} phone: {self.phone}"

    def get_username_or_id(self):
        return self.username if self.username else self.tg_id

