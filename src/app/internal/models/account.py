from django.db import models

from app.internal.models.user import User


class Account(models.Model):
    class Meta:
        verbose_name = "account"
        verbose_name_plural = "accounts"

    NUMBER_LENGTH = 4

    tg = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        to_field="tg_id"
    )
    number = models.CharField(max_length=NUMBER_LENGTH, unique=True, default="0"*NUMBER_LENGTH)
    name = models.TextField(default="Дебетовый счет")
    balance = models.DecimalField(default=0, decimal_places=2, max_digits=20)

    def __str__(self):
        return f"id: {self.id} account_number: {self.number} owner: {self.tg.get_username_or_id()} name: {self.name} balance: {self.balance}"
