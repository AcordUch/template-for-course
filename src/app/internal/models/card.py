from django.db import models

from app.internal.models.account import Account
from app.internal.models.user import User


class Card(models.Model):
    class Meta:
        verbose_name = "card"
        verbose_name_plural = "cards"

    NUMBER_LENGTH = 8

    tg = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        to_field="tg_id"
    )
    account = models.ForeignKey(
        Account,
        on_delete=models.CASCADE,
        to_field="id"
    )
    card_number = models.CharField(max_length=NUMBER_LENGTH, unique=True, default="0"*NUMBER_LENGTH)
    expiration_date = models.CharField(max_length=4)
    csc = models.CharField(max_length=3)

    def __str__(self):
        return (f"id: {self.id} owner: {self.tg.get_username_or_id()} account_id: {self.account.id} card_number: {self.card_number} expiration_date: {self.expiration_date}"
                f" csc: {self.csc} balance: {self.account.balance}")

    def description(self) -> str:
        return f"account_id: {self.account.id} card_number: {self.card_number} expiration_date: {self.expiration_date}"
