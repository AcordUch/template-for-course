from enum import Enum

from django.db import models

from app.internal.models.user import User
from app.internal.models.account import Account
from app.internal.models.card import Card


class PaymentOperation(models.Model):
    class Meta:
        verbose_name = "payment_history"
        verbose_name_plural = "payment_history's"

    amount = models.DecimalField(decimal_places=2, max_digits=20)
    timestamp = models.DateTimeField()

    from_user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        to_field="tg_id",
        related_name="from_users"
    )
    from_account = models.ForeignKey(
        Account,
        on_delete=models.CASCADE,
        to_field="number",
        null=True,
        related_name="from_accounts"
    )
    from_card = models.ForeignKey(
        Card,
        on_delete=models.CASCADE,
        to_field="card_number",
        null=True,
        related_name="from_cards"
    )

    to_user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        to_field="tg_id",
        related_name="to_users"
    )
    to_account = models.ForeignKey(
        Account,
        on_delete=models.CASCADE,
        to_field="number",
        null=True,
        related_name="to_accounts"
    )
    to_card = models.ForeignKey(
        Card,
        on_delete=models.CASCADE,
        to_field="card_number",
        null=True,
        related_name="to_cards"
    )

    def __str__(self):
        return f"Дата: {self.timestamp} От: {self.from_user.get_username_or_id()} К: {self.to_user.get_username_or_id()} Сумма: {self.amount}"


class OperationType(Enum):
    INCOME = 0
    EXPENSES = 1


class PaymentStats:
    def __init__(self):
        self.grouped: dict[OperationType, list[PaymentOperation]] = {
            OperationType.INCOME: [],
            OperationType.EXPENSES: []
        }
        self.income: int = 0
        self.expenses: int = 0

    def calculate_income_and_expenses(self, tg_id: int, history: list[PaymentOperation]):
        for operation in history:
            if operation.from_user.tg_id == tg_id:
                self._add_expenses(operation)
                self.expenses += operation.amount
            else:
                self._add_income(operation)
                self.income += operation.amount

        return self

    def _add_income(self, operation: PaymentOperation) -> None:
        self.grouped[OperationType.INCOME].append(operation)

    def _add_expenses(self, operation: PaymentOperation) -> None:
        self.grouped[OperationType.EXPENSES].append(operation)

    def get_income_history(self) -> list[PaymentOperation]:
        return self.grouped[OperationType.INCOME]

    def get_expenses_history(self) -> list[PaymentOperation]:
        return self.grouped[OperationType.EXPENSES]
