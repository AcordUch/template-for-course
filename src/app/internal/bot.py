from os.path import join as path_join

from environ import Env
from telegram.ext import Application, ApplicationBuilder, CommandHandler

import app.internal.transport.bot.handlers as handlers
from app.internal.transport.bot import domain
from config.settings import BASE_DIR, BOT_TOKEN, PASSWORD_SALT
from app.internal.services.password_service import PasswordService


def look_available_commands() -> str:
    return "\n".join(domain.COMMANDS_LIST)


def main():
    Env.read_env(path_join(BASE_DIR, '.env'))
    app: Application = ApplicationBuilder().token(token=BOT_TOKEN).build()
    password_service = PasswordService(PASSWORD_SALT)
    try:
        app.add_handler(CommandHandler("help", handlers.help(look_available_commands)))
        app.add_handler(CommandHandler("start", handlers.start))
        app.add_handler(CommandHandler("set_phone", handlers.set_phone(password_service)))
        app.add_handler(CommandHandler("me", handlers.me))
        app.add_handler(CommandHandler("delete_phone", handlers.delete_phone))
        app.add_handler(CommandHandler("delete", handlers.delete_user))
        app.add_handler(CommandHandler("get_accounts", handlers.get_accounts))
        app.add_handler(CommandHandler("get_cards", handlers.get_cards))
        app.add_handler(CommandHandler("balance_account", handlers.balance_account))
        app.add_handler(CommandHandler("balance_card", handlers.balance_card))
        app.add_handler(CommandHandler("money_transfer", handlers.money_transfer))
        app.add_handler(CommandHandler("look_fav", handlers.look_fav))
        app.add_handler(CommandHandler("add_fav", handlers.add_fav))
        app.add_handler(CommandHandler("remove_fav", handlers.remove_fav))
        app.add_handler(CommandHandler("statement", handlers.statement))
        app.add_handler(CommandHandler("balance_history", handlers.balance_history))

        print("log in")
        app.run_polling()
    finally:
        print("exit in progres...")


if __name__ == '__main__':
    main()
