from secrets import choice
from string import ascii_letters, digits
from hashlib import md5


class Password:
    def __init__(self, password: str, hash: str):
        self.password = password
        self.hash = hash


class PasswordService:
    _passwordLength = 7
    _alphabet = ascii_letters + digits

    def __init__(self, salt: str):
        self.salt = salt

    def generate_password(self) -> Password:
        password = ''.join(choice(self._alphabet) for _ in range(self._passwordLength))
        return Password(password, self.hashing(password))

    def hashing(self, password: str) -> str:
        hashed = md5((password + self.salt).encode())
        return hashed.hexdigest()