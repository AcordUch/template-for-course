from itertools import groupby
from decimal import Decimal
from datetime import datetime

import app.resources.bot as bot_texts
from app.models import Account, Card, User, PaymentOperation
from app.internal.models.payment_operation import PaymentStats

INDENT_DOUBLE = "\n\n"
INDENT_ONCE = "\n"


def info_about_user(user: User) -> str:
    return f"id: {user.tg_id}\n" \
           f"username: {user.username}\n" \
           f"first_name: {_mask_none(user.first_name)}\n" \
           f"last_name: {_mask_none(user.last_name)}\n" \
           f"full_name: {_mask_none(user.full_name)}\n" \
           f"link: {_mask_none(user.link)}\n" \
           f"phone: {user.phone}"


def _mask_none(value) -> str:
    return "" if value is None else value


def info_about_accounts(accounts: list[Account]) -> str:
    return bot_texts.ACCOUNTS__LIST.format("\n\n".join(map(info_about_account, accounts)))


def info_about_account(account: Account) -> str:
    return bot_texts.ACCOUNTS__DESCRIPTION.format(account.name, account.number, account.balance)


def info_about_cards(cards: list[Card]) -> str:
    card_by_account_name = groupby(
        sorted(cards, key=lambda card: card.account.name),
        key=lambda card: card.account.name
    )

    return "\n\n\n".join(
        [
            f"{bot_texts.CARDS__ACCOUNT.format(name)}\n{INDENT_DOUBLE.join([info_about_card(card) for card in cards])}"
            for name, cards in
            card_by_account_name
        ]
    )


def info_about_card(card: Card) -> str:
    return bot_texts.CARDS__DESCRIPTION.format(card.card_number, card.expiration_date, card.csc, card.account.balance)


def info_about_favorites(favorites: list[User]) -> str:
    return bot_texts.FAVORITE__LIST.format("\n".join(map(info_about_favorite, favorites)))


def info_about_favorite(fav: User) -> str:
    return f"\t- {fav.username if fav.username else fav.tg_id} ({fav.full_name})"


def info_about_statement(amount: Decimal, payments_stats: PaymentStats, statement_by: str) -> str:
    amount_on_start = amount + payments_stats.expenses - payments_stats.income
    return (
        f"{bot_texts.STATEMENT__HEADER.format(statement_by)}\n"
        f"{bot_texts.STATEMENT.format(amount, amount_on_start, payments_stats.income, payments_stats.expenses)}"
    )


def info_about_payment_history(payments_stats: PaymentStats) -> str:
    income = list(map(_info_about_income_payment_operation, payments_stats.get_income_history()))
    expenses = list(map(_info_about_expenses_payment_operation, payments_stats.get_expenses_history()))

    history = map(lambda pair: pair[0], sorted(income + expenses, key=lambda pair: pair[1]))

    return bot_texts.BALANCE_HISTORY__HEADER.format(INDENT_DOUBLE.join(history))


def _info_about_income_payment_operation(payments_operation: PaymentOperation) -> (str, datetime.timestamp):
    sender_name = payments_operation.from_user.username if payments_operation.from_user.username else payments_operation.from_user.id
    if payments_operation.to_card:
        return (
            bot_texts.BALANCE_HISTORY__LIST_INCOME_WITH_CARD.format(
                payments_operation.timestamp,
                payments_operation.to_account.number,
                payments_operation.to_card.card_number,
                sender_name,
                payments_operation.amount
            ),
            payments_operation.timestamp
        )
    else:
        return (
            bot_texts.BALANCE_HISTORY__LIST_INCOME.format(
                payments_operation.timestamp,
                payments_operation.to_account.number,
                sender_name,
                payments_operation.amount
            ),
            payments_operation.timestamp
        )


def _info_about_expenses_payment_operation(payments_operation: PaymentOperation) -> (str, datetime.timestamp):
    recipient_name = payments_operation.to_user.username if payments_operation.to_user.username else payments_operation.to_user.id
    return (
        bot_texts.BALANCE_HISTORY__LIST_EXPENSES.format(
            payments_operation.timestamp,
            payments_operation.from_account.number,
            payments_operation.from_card.card_number,
            recipient_name,
            payments_operation.amount
        ),
        payments_operation.timestamp
    )
