from decimal import Decimal
from datetime import datetime

from django.db.models import F, Q
from django.db import transaction

from app.internal.models.account import Account
from app.internal.models.card import Card
from app.internal.models.user import User
from app.internal.models.payment_operation import PaymentOperation
from app.internal.models.jwt_tokens import IssuedToken, AccessToken, RefreshToken


class DBIO:
    @staticmethod
    def create_or_update_user(
            username: str, first_name: str, last_name: str,
            full_name: str, tg_id: int, link: str, phone: str
    ):
        user: User = DBIO.get_user_by_tg_id(tg_id)
        User(
            id=user.id,
            username=username, tg_id=tg_id,
            first_name=first_name, last_name=last_name, full_name=full_name,
            link=link,
            phone=phone if user is None or phone != "None" else user.phone
            # во избежание потери телефона при обновлении
        ).save()

    @staticmethod
    def patch_user(
            tg_id: int, username: str | None, password: str | None,
            first_name: str | None, last_name: str | None, full_name: str | None,
            link: str | None, phone: str | None, email: str | None
    ):
        updates_raw = {
            "username": username,
            "password": password,
            "first_name": first_name,
            "last_name": last_name,
            "full_name": full_name,
            "link": link,
            "phone": phone,
            "email": email
        }
        updates = {k: v for k, v in updates_raw.items() if v is not None}
        User.objects.filter(tg_id=tg_id).update(**updates)

    @staticmethod
    def set_phone_to_user(tg_id: int, phone: str):
        User.objects.filter(tg_id=tg_id).update(phone=phone)

    @staticmethod
    def set_password_to_user(tg_id: int, pass_hash: str):
        User.objects.filter(tg_id=tg_id).update(password=pass_hash)

    @staticmethod
    def get_user_by_tg_id(tg_id: int) -> User:
        user: User = User.objects.prefetch_related("favorites").filter(tg_id=tg_id).first()
        if user is not None:
            user.phone = "None" if user.phone == "" or user.phone is None else user.phone
            # тк в админке можно установить пустой номер(см коммент в модели)
        return user

    @staticmethod
    def get_user_by_username(username: str) -> User:
        user: User = User.objects.prefetch_related("favorites").filter(username=username).first()
        if user is not None:
            user.phone = "None" if user.phone == "" or user.phone is None else user.phone
            # тк в админке можно установить пустой номер(см коммент в модели)
        return user

    @staticmethod
    def delete_user_by_id(tg_id: int) -> None:
        self = DBIO.get_user_by_tg_id(tg_id)
        User.delete(self)

    @staticmethod
    def get_account_by_id(account_id: int) -> Account:
        account: Account = Account.objects.select_related("tg").filter(id=account_id).first()
        return account

    @staticmethod
    def get_account_by_number(account_number: str) -> Account:
        account: Account = Account.objects.select_related("tg").filter(number=account_number).first()
        return account

    @staticmethod
    def get_card_by_card_number(card_number: str) -> Card:
        card: Card = Card.objects.select_related("tg", "account").filter(card_number=card_number).first()
        return card

    @staticmethod
    def get_all_accounts_by_tg_id(tg_id: int) -> list[Account]:
        accounts = Account.objects.select_related("tg").filter(tg_id=tg_id)
        return list(accounts)

    @staticmethod
    def get_first_account_by_tg_id(tg_id: int) -> Account:
        accounts = Account.objects.select_related("tg").filter(tg_id=tg_id).first()
        return accounts

    @staticmethod
    def add_account(account: Account) -> None:
        account.save()

    @staticmethod
    def get_all_cards_by_tg_id(tg_id: int) -> list[Card]:
        cards = Card.objects.select_related("tg", "account").filter(tg_id=tg_id)
        return list(cards)

    @staticmethod
    def add_card(card: Card) -> None:
        card.save()

    @staticmethod
    def delete_card_by_number(card_number: str) -> None:
        self = DBIO.get_card_by_card_number(card_number)
        Card.delete(self)

    @staticmethod
    def transfer_money(source_account_id: int, target_account_id: int, amount: Decimal) -> None:
        with transaction.atomic():
            Account.objects.filter(id=source_account_id).update(balance=F("balance") - amount)
            Account.objects.filter(id=target_account_id).update(balance=F("balance") + amount)

    @staticmethod
    def get_favorites(tg_id: int) -> list[User]:
        favorites = User.objects.get(tg_id=tg_id).favorites.all()
        return list(favorites)

    @staticmethod
    def add_fav(tg_id: int, user: User) -> None:
        User.objects.get(tg_id=tg_id).favorites.add(user)

    @staticmethod
    def remove_fav(tg_id: int, user: User) -> None:
        User.objects.get(tg_id=tg_id).favorites.remove(user)

    @staticmethod
    def get_user_from_favorites_by_tg_id(tg_id: int, fav_tg_id: int) -> User | None:
        if User.objects.filter(tg_id=tg_id, favorites__tg_id=fav_tg_id).values_list("favorites__tg_id").exists():
            return User.objects.get(tg_id=fav_tg_id)

        return None

    @staticmethod
    def get_user_from_favorites_by_username(tg_id: int, fav_username: str) -> User | None:
        if (User.objects
                .filter(tg_id=tg_id, favorites__username=fav_username)
                .values_list("favorites__username")
                .exists()):
            return User.objects.get(username=fav_username)

        return None

    @staticmethod
    def add_record_to_payment_history(
            amount: Decimal,
            timestamp: datetime.timestamp,
            from_user: User,
            from_account: Account,
            from_card: Card | None,
            to_user: User,
            to_account: Account,
            to_card: Card | None,
    ):
        PaymentOperation(
            amount=amount,
            timestamp=timestamp,
            from_user=from_user,
            from_account=from_account,
            from_card=from_card,
            to_user=to_user,
            to_account=to_account,
            to_card=to_card
        ).save()

    @staticmethod
    def get_records_from_payment_history(tg_id: int) -> list[PaymentOperation]:
        history = (
            PaymentOperation.objects
            .select_related(
                "from_user",
                "from_account",
                "from_card",
                "to_user",
                "to_account",
                "to_card"
            )
            .filter((Q(from_user=tg_id) | Q(to_user=tg_id)))
        )
        return list(history)

    @staticmethod
    def get_records_from_payment_history_by_account(tg_id: int, account_number: str) -> list[PaymentOperation]:
        history = (
            PaymentOperation.objects
            .select_related(
                "from_user",
                "from_account",
                "from_card",
                "to_user",
                "to_account",
                "to_card"
            )
            .filter((Q(from_user=tg_id) | Q(to_user=tg_id)) & (Q(from_account=account_number) | Q(to_account=account_number)))
        )
        return list(history)

    @staticmethod
    def get_records_from_payment_history_by_card_number(tg_id: int, card_number: str) -> list[PaymentOperation]:
        history = (
            PaymentOperation.objects
            .select_related(
                "from_user",
                "from_account",
                "from_card",
                "to_user",
                "to_account",
                "to_card"
            )
            .filter((Q(from_user=tg_id) | Q(to_user=tg_id)) & (Q(from_card=card_number) | Q(to_card=card_number)))
        )
        return list(history)

    @staticmethod
    def add_jwt_token(token: AccessToken | RefreshToken):
        user = DBIO.get_user_by_tg_id(int(token.id))
        IssuedToken(jti=token.jti, user=user, expired=token.expired, is_access_token=token.is_access_token).save()

    @staticmethod
    def check_jwt_token_valid(jti: str) -> bool:
        token = IssuedToken.objects.values_list("revoked").filter(jti=jti).first()
        return token is not None and not token[0]

    @staticmethod
    def check_jwt_is_access_token(jti: str) -> bool:
        token = IssuedToken.objects.values_list("is_access_token").filter(jti=jti).first()
        return token is not None and token[0]

    @staticmethod
    def revoked_jwt_token(jti: str) -> None:
        IssuedToken.objects.filter(jti=jti).update(revoked=True)

    @staticmethod
    def revoked_jwt_token_by_tg_id(tg_id: int) -> None:
        IssuedToken.objects.filter(user_id=tg_id).update(revoked=True)
