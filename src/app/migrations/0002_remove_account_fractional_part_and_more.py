# Generated by Django 4.2.13 on 2024-05-15 16:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='account',
            name='fractional_part',
        ),
        migrations.RemoveField(
            model_name='account',
            name='integer_part',
        ),
        migrations.AddField(
            model_name='account',
            name='balance',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=20),
        ),
        migrations.AddField(
            model_name='account',
            name='number',
            field=models.CharField(default='stub', max_length=4),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='card',
            name='card_number',
            field=models.CharField(max_length=8),
        ),
    ]
