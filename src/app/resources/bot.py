HELP_MESSAGE = "Список доступных комманд:\n{}"

REGISTRATION__ONBOARDING = 'Привет, я сохранил о тебе кое какую информацию. Теперь, что бы продолжить, введи свой номер телефона в формате "+7..." через команду /set_phone <номер>'
REGISTRATION__UNREGISTERED = 'Введите /start для начала общения'
REGISTRATION__PHONE_NEED_REQUEST = 'Сначала необходимо ввести номер телефона'
REGISTRATION__PHONE_INPUT_FORMATE = 'Введите номер телефона в одном сообщении с командой через пробел в формате "+7...", например "/set_phone <номер>"'
REGISTRATION__PHONE_INCORRECT = 'Некорректный номер телефона. Номер телефона должен иметь вид "+7..."'
REGISTRATION__PHONE_ALREADY_SET = 'Номер телефона же установлен'
REGISTRATION__PHONE_SAVE = 'Номер телефона сохранён'
REGISTRATION__PASSWORD = 'Ваш пароль: {}'

MAIN_MENU__PHONE_DELETED = 'Номер телефона удален'
MAIN_MENU__USER_DELETED = 'Информация о вас удалена. Для начала использования бота введите /start'

ACCOUNTS__LIST = 'Ваши счета:\n{}'
ACCOUNTS__DESCRIPTION = 'Название: {}; Номер счёта: {}; Баланc: {}'
ACCOUNT__BALANCE = 'На вашем счете {} рублей'
ACCOUNT__BALANCE__NEED_ID = 'Введите номер счета в одном сообщении с командой через пробел, например "/balance_account <номер>"'
ACCOUNT__BALANCE__NOT_EXIST = 'Данного счета не существует'


CARDS__ACCOUNT = 'Привязана к {}:'
CARDS__DESCRIPTION = 'Номер карты: {}; До: {}; CSC: {}; Баланc: {}'
CARD__BALANCE = 'На вашей карте {} рублей'
CARD__BALANCE__NEED_ID = 'Введите номер карты в одном сообщении с командой через пробел, например "/balance_card <номер>"'
CARD__BALANCE__NOT_EXIST = 'Данной карты не существует'

MONEY_TRANSFER__NOT_ENOUGH_ARGS = 'Введите получателя перевода(номер карты/счета или логин пользоавтеля) и сумму в одном сообщении с командой через пробел, например "/money_transfer <номер> <сумма>"'
MONEY_TRANSFER__UNSPECIFIED_TARGET = 'Некорректный идентификатор получателя перевода'
MONEY_TRANSFER__VALUE_ERROR = 'Некорректная сумма перевода'
MONEY_TRANSFER__TARGET_NOT_EXIST = 'Получатель не найден'
MONEY_TRANSFER__NOT_ENOUGH_MONEY = 'На ваших счётах недостаточно средств'
MONEY_TRANSFER__SUCCESSFUL = 'Перевод успешно выполнен!'

FAVORITE__EMPTY = 'Список избранных пользователей пустой'
FAVORITE__LIST = 'Список избранных пользователей:\n{}'
FAVORITE__ADD__NOT_ENOUGH_ARGS = 'Введите пользователя в одном сообщении с командой через пробел, например "/add_fav <username/ID>"'
FAVORITE__ADD__NOT_EXIST = 'Указаный пользователь не существует'
FAVORITE__ADD__SUCCESSFUL = 'Пользователь успешно добавлен в избранное'
FAVORITE__REMOVE__NOT_ENOUGH_ARGS = 'Введите пользователя в одном сообщении с командой через пробел, например "/remove_fav <username/ID>"'
FAVORITE__REMOVE__NOT_EXIST = 'Указанного пользователя нет в избранных'
FAVORITE__REMOVE__SUCCESSFUL = 'Пользователь удален из избранного'

STATEMENT__ADD__NOT_ENOUGH_ARGS = 'Введите номер счёта/карты в одном сообщении с командой через пробел, например "/statement <number>"'
STATEMENT__UNSPECIFIED_TARGET = 'Некорректный идентификатор'
STATEMENT__TARGET_NOT_FOUND = '{} отсутствуют'
STATEMENT__HEADER = 'Выписка по {}'
STATEMENT = '\tБаланс: {}\n\tБаланс на начало периода: {}\n\tПоступления: {}\n\tРасходы: {}'

BALANCE_HISTORY__HEADER = 'Операции по счету/карте:\n\n{}'
BALANCE_HISTORY__LIST_EMPTY = 'Не найдено операций'
BALANCE_HISTORY__LIST_INCOME = 'Входящий перевод; Дата операции: {}; Перевод выполнен на счёт: {}; Отправитель: {}; Сумма операции: {}'
BALANCE_HISTORY__LIST_INCOME_WITH_CARD = 'Входящий перевод; Дата операции: {}; Перевод выполнен на счёт: {}, карту: {}; Отправитель: {}; Сумма операции: {}'
BALANCE_HISTORY__LIST_EXPENSES = 'Исходящий перевод; Дата операции: {}; Перевод выполнен со счёта: {}, карты: {}; Получатель: {}; Сумма операции: {}'
