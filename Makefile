dev:
	python src/manage.py runserver localhost:8000

run_bot:
	python src/manage.py run_bot

runall --jobs=2: run_bot dev

migrate:
	python src/manage.py migrate

makemigrations:
	python src/manage.py makemigrations \
	sudo chown -R ${USER} src/app/migrations/

createsuperuser:
	python src/manage.py createsuperuser

collectstatic:
	python src/manage.py collectstatic --no-input

command:
	python src/manage.py ${c}

shell:
	python src/manage.py shell

debug:
	python src/manage.py debug

piplock:
	pipenv install
	sudo chown -R ${USER} Pipfile.lock

lint:
	isort .
	flake8 --config setup.cfg
	black --config pyproject.toml .

check_lint:
	isort --check --diff .
	flake8 --config setup.cfg
	black --check --config pyproject.toml .

dockerBuildRun: dockerBuild dockerRun

dockerGetRun: dockerGet dockerRun

dockerBuildPush: dockerBuild dockerPush

dockerUpdate: dockerStop dockerRemove dockerGetRun

dockerBuild:
	docker build -t registry.gitlab.com/acorduch/template-for-course/dt-backend .

dockerPush:
	docker push registry.gitlab.com/acorduch/template-for-course/dt-backend

dockerRun:
	docker run -p 8000:8000/tcp -p 8000:8000/udp --name dt-backend registry.gitlab.com/acorduch/template-for-course/dt-backend:latest

dockerGet:
	docker pull registry.gitlab.com/acorduch/template-for-course/dt-backend:latest

dockerStop:
	docker container stop dt-backend

dockerRemove:
	docker container rm dt-backend